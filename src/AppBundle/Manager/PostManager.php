<?php
namespace AppBundle\Manager;

use AppBundle\Entity\Post;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class PostManager
 * @package AppBundle\Manager
 */
class PostManager
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param $page
     */
    public function findLatest($page) {
        return $this->entityManager->getRepository(Post::class)->findLatest($page);
    }

    /**
     * @param $query
     */
    public function findBySearchQuery($query) {
        return $this->entityManager->getRepository(Post::class)->findBySearchQuery($query);
    }

     /**
     * @param array $parameters
     * @param array $option
     */
    public function findByAuthor(User $user) {
        return $this->entityManager->getRepository(Post::class)->findBy(['author' => $user], ['publishedAt' => 'DESC']);
    }
}
