Symfony Demo Application
========================

**Source:** https://github.com/thinkfab/symfony_demo_3.4

```bash
# 1. Cloning the project.
$ git clone https://gitlab.com/Natizyskunk/symfony_demo_3.4.git
$ cd symfony_demo_3.4/

# 2. Installing composer and yarn required packages.
$ composer install && yarn install

# 3. Using doctrine to generate the database and populate it with test data.
$ php bin/console doctrine:database:create
$ php bin/console doctrine:schema:create
$ php bin/console doctrine:fixtures:load

# 4. Building the site:
# 4.a - For development.
$ yarn encore dev
# 4.b - Or for production.
$ yarn encore production

# 5. Let's run to see the magic!
$ php bin/console server:run
```